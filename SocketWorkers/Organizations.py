import json
from sqlite3 import IntegrityError

from SocketWorkers.BaseSocketWorker import BaseClientSocketWorker
from database.DataBaseWorkers import OrganizationDBWorker
from config.values import HOST_ORGANIZATIONS, PORT_ORGANIZATIONS

class OrganizationsSocketWorker(BaseClientSocketWorker):
    '''
    Воркер для работы с сервисом организаций - умеет только запрашивать обновления, большего от него и не требуется
    '''
    def __init__(self):
        super().__init__(HOST_ORGANIZATIONS, PORT_ORGANIZATIONS)

    def update(self, city:str) -> dict:
        self.connect()
        self.send(message='{"Table": "Point", "Query": "Select", "TypeParameter": "AllCity", "Values":["%s"]}' % city)
        response = self.read(expected_sequence=True)
        self.disconnect()

        if response != None:
            organizationsDB = OrganizationDBWorker()
            for data in [json.loads(store) for store in response]:
                try:
                    organizationsDB.insert(data)
                except IntegrityError:
                    organizationsDB.update(data)
                except Exception as ex:
                    raise Exception("Can't insert/update datas", ex.args)
            organizationsDB.close()
        return {"success": True}

if __name__ == "__main__":
    organizationsWorker = OrganizationsSocketWorker()
    organizationsWorker.update("Курган")

# Hash                : 8d530b253ab52715733e6d92caa19cbcf5edfb43f63311970d3ed5af7265763302a2a29843231753616c4f8b64a8d1602800ea2303caca35477ff6bc09d15231
# HashOrg             : a39aae3dc6e2b1ba474e1b80f8acedb8d21bd57d418d73297b1b0b20782f8ee99c47509f3bc5d4a0059455b92bcf01e2171dacd1f35789a81ddf643c8e7d5180
# City                : Курган
# Street              : 5 микрорайон
# House               : 33
# CreateTime          : 2017-04-11T09:46:50Z
# CreateTimeStr       : 2017-04-11 09:46:50
# NameSklad           : SkladMicroFive
# MinOrderAmount      : 0
# Active              : True
# Lat                 : 55.466239
# Lon                 : 65.274463