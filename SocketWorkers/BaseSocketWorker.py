import socket, ssl
from config.values import PATH_TO_CERT_FILE, PATH_TO_KEY_FILE
from SocketWorkers.CustomSocketTools import readMessage, calculateMessageLength

class BaseClientSocketWorker:
    '''
    Базовый класс для создания клиент-воркеров с конкретными соединениями
    '''
    def __init__(self, host:str, port:int):
        self.host = host
        self.port = port
        self._connection = ssl.wrap_socket(socket.socket(), certfile=PATH_TO_CERT_FILE,
                                     keyfile=PATH_TO_KEY_FILE)

    def connect(self):
        self._connection.connect((self.host, self.port))
    def disconnect(self):
        self._connection.close()

    def read(self, **kwargs):
        return readMessage(self._connection, **kwargs)

    def send(self, message: str):
        """
        Создаёт подключение к указанному ip по указанному порту,
        отправляет сообщение, ждёт ответ, получив ответ закрывает соединение и возвращает ответ.
        Предполагается два вида ответа:
        - ответ содержится в одном сообщении
        - ответ состоит из последовательности сообщений
        Для получения одного сообщения достаточно вызвать функцию без указания дополнительных параметров.
        Для получения последовательности сообщений необходимо указать дополнительный параметр функции expected_sequence=True
        """
        request = calculateMessageLength(message)
        self._connection.send(request)

if __name__ == "__main__":
    from config.values import HOST_ORGANIZATIONS, PORT_ORGANIZATIONS
    netconn = BaseClientSocketWorker(HOST_ORGANIZATIONS, PORT_ORGANIZATIONS)
    netconn.connect()
    netconn.send('{"Table": "Point", "Query": "Select", "TypeParameter": "AllCity", "Values":["Курган"]}')
    print(netconn.read(expected_sequence=True))
    netconn.disconnect()