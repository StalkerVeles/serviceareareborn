from socket import socket

'''
кастомный набор инструментов для работы с сетью (приём/передача данных)
'''

def calculateMessageLength(message:str) -> bytes:
    """
    Возвращает сообщение с прикреплённым заголовком, который сообщает длину тела сообщения
    """
    lm = str(len(message.encode()))
    lm = lm + ":" + message
    return lm.encode()

def reader(conn:socket) -> str:
    """
    Из переданного сокет-соединения принимает сообщение.
    Сначала читает последовательность символов до первого двоеточния -
        это заголовок сообщения, который должен содержать только цифры (длина тела сообщения в байтах)
    Далее принимает столько байт сколько получилось узнать из заголовка.
    """
    lenMessage = ""
    data_in = conn.recv(1)
    while (data_in.decode() != ":"):
        lenMessage += data_in.decode()
        data_in = conn.recv(1)

    data_in += conn.recv(int(lenMessage))
    data_in = data_in.decode()
    return data_in[1:]

def readMessage(conn:socket, **kwargs) -> str or None:
    '''
    Читает сообщение из указанного соединения.
    В kwargs можно указать expected_sequence = True
    и читатель будет считывать данные порционно до сообщения EOF.
    (01:EOF - 01 рудимент нашей системы, не знаю когда его уберут,
    все задачи имеют статус "ещё вчера надо было", поэтому код-ревью
    в некоторых частях отдела очень буксует. Так что оставляю 01:EOF
    на всякий пожарный, не мешает.)
    '''
    response = None
    if kwargs:
        if "expected_sequence" in kwargs.keys():
            if kwargs["expected_sequence"]:
                response = []
                while 1:
                    resp_message = reader(conn)
                    if resp_message != "01:EOF" or resp_message != "EOF":
                        if resp_message[0:2] == "01":
                            response.append(resp_message[3:])
                        else:
                            response = None
                            break
                    else:
                        break
            else:
                response = reader(conn)
    else:
        response = reader(conn)
    return response