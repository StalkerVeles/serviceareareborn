from database.DBConnector import DB, rollbackable
import json
from collections import namedtuple

class AreasDBWorker(DB):
    '''
    Модель таблицы areas.
    Таблица areas хранит в себе координаты разметки зон обслуживания,
    работа с таблицей производится через данную модель.
    Суть методов ясна, поэтому описывать их не буду.
    '''
    def __init__(self):
        super().__init__()
        self.conn.executescript("""CREATE TABLE IF NOT EXISTS areas (
                                                Name STRING PRIMARY KEY,
                                                City STRING,
                                                Coordinates STRING)""")
        self.conn.commit()

    @rollbackable
    def insert(self, city:str, area_name:str, coordinates:str):
        self.conn.execute("INSERT INTO areas (Name, City, Coordinates) VALUES (?, ?, ?)", (area_name.lower(), city.lower(), coordinates))
        self.conn.commit()

    @rollbackable
    def update(self, area_name:str, coordinates:str):
        self.conn.execute("UPDATE areas SET Coordinates=:coordinates WHERE Name=:name", {"name": area_name.lower(), "coordinates":coordinates})
        self.conn.commit()

    @rollbackable
    def delete(self, area_name:str):
        self.conn.execute("DELETE FROM areas WHERE Name=?", (area_name.lower(),))
        self.conn.commit()

    def select(self, city:str):
        query = self.conn.execute("SELECT Name, Coordinates FROM areas WHERE City=?", (city.lower(),))
        result = {}
        # данное выражение формирует словарь вида
        # {'имя зоны обслуживания': 'координаты разметки'
        # 'имя зоны обслуживания2': 'координаты разметки2'}
        [result.update({area[0]: json.loads(area[1])}) for area in query.fetchall()]
        return result

# данный именованный кортеж нужен для представления инфы об организации в удобном виде
# плюс если набор данных будет дополнен в будущем, то это не сломает программу в тех местах
# где данные изменения не нужны
Organization = namedtuple("Organization", ["Hash", "HashOrg", "City", "Street", "House", "Lat", "Lon"])

class OrganizationDBWorker(DB):
    '''
    Модель таблицы stores.
    Таблица stores хранит в себе информацию об организациях,
    работа с таблицей производится через данную модель.
    Суть методов ясна, поэтому описывать их не буду.
    '''
    def __init__(self):
        super().__init__()
        self.conn.executescript("""CREATE TABLE IF NOT EXISTS stores (
                                                Hash STRING PRIMARY KEY,
                                                HashOrg STRING,
                                                City STRING,
                                                Street STRING,
                                                House STRING,
                                                Lat STRING,
                                                Lon STRING)""")
        self.conn.commit()

    @rollbackable
    def insert(self, datas:dict):
        datas.update({"City": datas["City"].lower(), "Street": datas["Street"].lower()})
        self.conn.execute("INSERT INTO stores (Hash, HashOrg, City, Street, House, Lat, Lon) VALUES (:Hash, :HashOrg, :City, :Street, :House, :Lat, :Lon)", datas)
        self.conn.commit()

    @rollbackable
    def update(self, datas:dict):
        datas.update({"City": datas["City"].lower(), "Street": datas["Street"].lower()})
        self.conn.execute("UPDATE stores SET Hash=:Hash, HashOrg=:HashOrg, City=:City, Street=:Street, House=:House, Lat=:Lat, Lon=:Lon WHERE Hash=:Hash", datas)
        self.conn.commit()

    def select(self, city:str, street:str, house:str):
        query = self.conn.execute("SELECT * FROM stores WHERE City=? AND Street=? AND House=?",
                          (city.lower(), street.lower(), house.lower())).fetchone()
        return Organization(query[0], query[1], query[2], query[3], str(query[4]), query[5], query[6]) if query != None else query

    @rollbackable
    def delete(self, **kwargs):
        if kwargs:
            if "Hash" in kwargs:
                self.conn.execute("DELETE FROM stores WHERE Hash=:Hash", kwargs)
                self.conn.commit()
                return True
            elif "City" and "Street" and "House" in kwargs:
                [kwargs.update({k: str(v).lower()}) for k, v in kwargs.items()]
                self.conn.execute("DELETE FROM stores WHERE City=:City AND Street=:Street AND House=:House", kwargs)
                self.conn.commit()
                return True
            else:
                return False
        else:
            return False


if __name__ == "__main__":
    pass