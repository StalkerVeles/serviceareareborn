# -*- coding:utf-8 -*-

import sys, os, json
abspath = os.path.dirname(os.path.abspath(__file__))
abspath = abspath[0:abspath.rfind("/")]
sys.path.append(os.path.join(abspath, ".."))

import unittest
from database.DataBaseWorkers import AreasDBWorker, OrganizationDBWorker

class TestDataBase(unittest.TestCase):
    def test_areas(self):
        areasWorker = AreasDBWorker()
        areasWorker.insert("Киров_тест", "Преображенская, 43", json.dumps([[49.65666616900573,58.61467193329283],[49.65823794348843,58.593710656469625],[49.68165899260654,58.59406352170967],[49.679765353042,58.61534085143637],[49.65666616900573,58.61467193329283]]))

        areas = areasWorker.select("Киров_тест")
        self.assertTrue(isinstance(areas, dict))
        self.assertTrue(len(areas) == 1)
        self.assertTrue("преображенская, 43" in areas.keys())
        self.assertEqual(areas["преображенская, 43"], [[49.65666616900573, 58.61467193329283], [49.65823794348843, 58.593710656469625], [49.68165899260654, 58.59406352170967], [49.679765353042, 58.61534085143637], [49.65666616900573, 58.61467193329283]])

        areasWorker.update("преображенская, 43", json.dumps([[49.65666616900573, 58.61467193329283], [49.65823794348843, 58.593710656469625], [49.68165899260654, 58.59406352170967], [49.65666616900573, 58.61467193329283]]))
        areas = areasWorker.select("Киров_тест")
        self.assertTrue(len(areas) == 1)
        self.assertTrue("преображенская, 43" in areas.keys())
        self.assertEqual(areas["преображенская, 43"], [[49.65666616900573, 58.61467193329283], [49.65823794348843, 58.593710656469625], [49.68165899260654, 58.59406352170967], [49.65666616900573, 58.61467193329283]])

        areasWorker.delete("преображенская, 43")
        areas = areasWorker.select("Киров_тест")
        self.assertEqual(areas, {})
        areasWorker.close()

    def test_organisations(self):
        organizations = OrganizationDBWorker()
        new_org = {'Hash': '1d23', 'HashOrg': 'abc', 'City': 'Курган', 'Street': 'Максима горького', 'House': '61',
                   'CreateTime': '2017-04-11T09:46:50Z', 'CreateTimeStr': '2017-04-11 09:46:50',
                   'NameSklad': 'SkladMicroFive', 'MinOrderAmount': 0, 'Active': True, 'Lat': '55.498765',
                   'Lon': '65.288888'}
        organizations.insert(new_org)

        self.assertEqual(organizations.select("Курган", "Максима Горького", "61").Hash, "1d23")
        self.assertEqual(organizations.select("Курган", "Максима Горького", "61").House, "61")

        organizations.update({"Hash": "1d23", "HashOrg": "abcd", "City":"Курган", "Street": "максима горького", "House": "56 б", "Lat": "55.432535", "Lon": "65.123423"})
        self.assertEqual(organizations.select("Курган", "Максима Горького", "61"), None)
        self.assertEqual(organizations.select("Курган", "Максима Горького", "56 б").Lat, 55.432535)
        self.assertEqual(organizations.select("Курган", "Максима Горького", "56 б").Lon, 65.123423)
        self.assertEqual(organizations.select("Курган", "Максима Горького", "56 б").House, "56 б")

        self.assertTrue(organizations.delete(**{'Hash': '1d23'}))

        new_org = {'Hash': '1d23', 'HashOrg': 'abc', 'City': 'Курган', 'Street': 'Максима горького', 'House': '61',
                   'CreateTime': '2017-04-11T09:46:50Z', 'CreateTimeStr': '2017-04-11 09:46:50',
                   'NameSklad': 'SkladMicroFive', 'MinOrderAmount': 0, 'Active': True, 'Lat': '55.498765',
                   'Lon': '65.288888'}
        organizations.insert(new_org)
        self.assertTrue(organizations.delete(**{'City': 'Курган', 'Street': 'максима Горького', 'House': '61'}))
        new_org = {'Hash': '1d23', 'HashOrg': 'abc', 'City': 'Курган', 'Street': 'Максима горького', 'House': '61',
                   'CreateTime': '2017-04-11T09:46:50Z', 'CreateTimeStr': '2017-04-11 09:46:50',
                   'NameSklad': 'SkladMicroFive', 'MinOrderAmount': 0, 'Active': True, 'Lat': '55.498765',
                   'Lon': '65.288888'}
        organizations.insert(new_org)
        self.assertFalse(organizations.delete(**{'City': 'Курган', 'Street': 'максима Горького'}))

        self.assertEqual(organizations.select("Курган", "Максима Горького", "61").Hash, "1d23")
        self.assertTrue(organizations.delete(**{'City': 'Курган', 'Street': 'максима Горького', 'House': '61'}))
        self.assertEqual(organizations.select("Курган", "Максима Горького", "61"), None)

        organizations.close()


if __name__ == "__main__":
    unittest.main()