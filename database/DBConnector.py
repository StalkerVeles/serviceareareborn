import sqlite3
from config.values import DB_PATH
import functools

class DB(object):
    '''
    Базовый класс для определения новых моделей таблиц базы данных
    '''
    def __init__(self):
        self.conn = sqlite3.connect(DB_PATH)

    def insert(self, *args):
        pass

    def update(self, *args):
        pass

    def close(self):
        self.conn.close()

    def __del__(self):
        self.close()

def rollbackable(func):
    '''
    данный декоратор оборачивает методы моделей в перехватчик исключений с автоматическим роллбэком
    --- не писать же роллбэк каждый раз вручную
    '''
    @functools.wraps(func)
    def decorate(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except sqlite3.IntegrityError:
            args[0].conn.rollback()
            raise sqlite3.IntegrityError
        except:
            args[0].conn.rollback()
            raise Exception
    return decorate