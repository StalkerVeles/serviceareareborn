import os

# certs
KEY_FILE = "pemkey.key"
CERT_FILE = "certificate.pem"
PATH_TO_KEY_FILE = "{0}/{1}/{2}".format(os.path.abspath(os.path.dirname(__file__)), "keys", KEY_FILE)
PATH_TO_CERT_FILE = "{0}/{1}/{2}".format(os.path.abspath(os.path.dirname(__file__)), "certs", CERT_FILE)

# ip_and_port
MY_IP = "0.0.0.0"
MY_PORT = 12730
HOST_ORGANIZATIONS = "91.240.87.193"
# HOST_ORGANIZATIONS = "127.0.0.1"
PORT_ORGANIZATIONS = 50055

# database
DB_NAME = "db.sqlite"
__path = os.path.dirname(__file__)
DB_PATH = "{0}/{1}/{2}".format(os.path.abspath(__path[0:__path.rfind("/")]), "database", DB_NAME)

# tokens
OAuth_Yandex = "AQAAAAAQzybMAAUbZeh9bSD1i08XtE8cX2AQV_Y"