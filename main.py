# -*- coding:utf-8 -*-

import json
import socket
import socketserver
import ssl
from types import MappingProxyType

from SocketWorkers.CustomSocketTools import readMessage, calculateMessageLength
from config.values import PATH_TO_CERT_FILE, PATH_TO_KEY_FILE, MY_IP, MY_PORT, DB_PATH


class ThreadedTCPRequestHandler(socketserver.BaseRequestHandler):
    def handle(self):
        requestJSON = readMessage(self.request)
        if requestJSON == "PING":
            self.request.sendall(calculateMessageLength("PONG"))
        else:
            try:
                request = MappingProxyType(json.loads(requestJSON))
                if ("type" in request.keys()):
                    if request["type"] == "UpdateOrgs":
                        pass # проивзодится процедура обновления информации об организациях
                    elif request["type"] == "UpdateAreas":
                        pass # производится процедура обновления разметки зон обслуживания для указанного города
                    elif request["type"] == "IdentifyServiceStore":
                        pass # производится процедура определения магазина обслуживания для присланного адреса
            except:
                self.request.sendall(calculateMessageLength(json.dumps({'success': False, 'error': 'can`t unjson request'})))



class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    def __init__(self, myaddress, handler, bind_and_activate=True):
        socketserver.TCPServer.__init__(self, myaddress, handler)
        self.logRequests = True
        # simple comment 1
        self.socket = ssl.wrap_socket(socket.socket(),
                                      server_side=True,
                                      certfile=PATH_TO_CERT_FILE,
                                      keyfile=PATH_TO_KEY_FILE,
                                      ca_certs=PATH_TO_CERT_FILE,
                                      cert_reqs=ssl.CERT_REQUIRED,
                                      ssl_version=ssl.PROTOCOL_TLSv1_2)

        if bind_and_activate:
            self.server_bind()
            self.server_activate()

    def shutdown_request(self, request):
        request.shutdown(socket.SHUT_WR)

if __name__ == "__main__":
    server = ThreadedTCPServer((MY_IP, MY_PORT), ThreadedTCPRequestHandler)
