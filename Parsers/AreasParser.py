from Parsers.YandexDiskFileDownloader import YandexDiskFileDownloader
from config.values import OAuth_Yandex
from database.DataBaseWorkers import AreasDBWorker
from sqlite3 import IntegrityError
import json

def getAreas(city:str) -> dict or None:
    yandexConn = YandexDiskFileDownloader(OAuth_Yandex)
    files = yandexConn.getFilesFromFolder("/Конструктор карт Яндекса/Экспорт/") # Получает с яндекс.диска файлы
    target_file = getTargetFileUrl(files, city) # Выбирает нужный файл
    if target_file != None:
        return yandexConn.downloadFile( target_file, type="json") # Скачивает выбранный файл
    else:
        return None

def getTargetFileUrl(files:tuple or list, word_in_filename:str) -> str:
    '''
    получает набор "файлов" и слово, которое должно содержаться в имени файла (либо быть именем файла)
    слово нужно для выбора конкретных файлов, в данном случае будут выбираться файлы
    с названием города в имени, таким образом отмечаются файлы с разметками зон доставки
    по городам
    '''
    word_in_filename = word_in_filename.lower()
    urls = {}
    # формирует в словаре набор в виде {'дата_модификации_файла': 'url_для_скачивания'}
    [urls.update({i["modified"]: i["file"]}) for i in files if word_in_filename in i["name"].lower()]
    # выбирает из словаря самый поздний ключ (который является датой со временем)
    return [urls[max(urls.keys())] if urls != {} else None][0]

def separateDatas(areas:dict):
    '''
    формирует из полученных данных словарь только с необходимыми данными
    {'название зоны1': 'координаты зоны1',
    '...': '...',
    'название зоныN': 'координаты зоныN'}
    '''
    clean_datas = {}
    if areas != None:
        [clean_datas.update({f["properties"]["description"]: f["geometry"]["coordinates"][0]})
                                for f in areas["features"]
                                    if f["geometry"]["type"] == "Polygon"]
    return clean_datas

def updateAreas(city:str):
    '''
    получает данные по зонам доставки и либо добавляет новую запись в базу данных,
    либо, если такая запись уже имеется, то обновляет её
    нужно ещё добавить автоматическое сравнение текущего набора данных и нового
    и в случае если в текущем есть что-то, чего нет в новом, то удалять такую запись... если это целесообразно (ПРОДУМАТЬ!!!)
    --- пока что оставлю для этого только вариант с явным удалением зон обслуживания
    '''
    areas = separateDatas(getAreas(city))
    if len(areas) > 0:
        areasWorker = AreasDBWorker()
        for area, points in areas.items():
            coordinates = json.dumps([p for p in points])
            try:
                areasWorker.insert(city, area, coordinates)
            except IntegrityError:
                areasWorker.update(area, coordinates)

if __name__ == "__main__":
    updateAreas("Курган")