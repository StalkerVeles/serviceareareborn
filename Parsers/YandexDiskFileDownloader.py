from config.values import OAuth_Yandex
from grab import Grab

class YandexDiskFileDownloader(object):
    '''
    Модуль для работы с яндекс.диском
    '''
    def __init__(self, OAuth:str):
        '''
        Яндекс работает по OAuth-авторизации, поэтому при создании
        экземпляра класса нужно передать ему ключ OAuth
        '''
        self.g = Grab()
        self.g.setup(headers={"Authorization": "OAuth %s" % OAuth, "Depth": "1"})
        self._pathURL = "https://cloud-api.yandex.net/v1/disk/resources/?path=%s"

    def getFilesFromFolder(self, folder:str) -> tuple:
        '''
        Получить информацию о содержимом указанного каталога на яндекс.диске
        '''
        self.g.go(url=self._pathURL % folder)
        if "error" not in self.g.doc.json:
            return tuple([i for i in self.g.doc.json['_embedded']['items']])
        else:
            raise Exception("Yandex answered with error", self.g.doc.json)

    def downloadFile(self, url:str, **kwargs):
        '''
        Скачивает файл по url.
        Можно дополнительно указать ожидаемый тип файла json и функция попытается вернуть
        ответ в видел json. Если указать bytes, то вернётся ответ в байтах (этот же вариант
        работает по-умолчанию).
        '''
        self.g.go(url=url)
        if kwargs:
            if "type" in kwargs.keys():
                if kwargs["type"] == "json":
                    try:
                        return self.g.doc.json
                    except: # если нет json содержимого, то будет возбуждено исключение
                        raise Exception("Can't unjason", self.g.doc.body)
                elif kwargs["type"] == "bytes":
                    # вернуть набор байтов
                    return self.g.doc.body
                else:
                    # если указан необрабатываемый параметр, то будет возбуждено исключение
                    raise Exception("Undefined type", kwargs["type"])
        else:
            # вернуть набор байтов
            return self.g.doc.body

if __name__ == "__main__":
    # нормальные тесты ещё не подъехали
    try:
        yandexConn = YandexDiskFileDownloader(OAuth_Yandex)
        files = yandexConn.getFilesFromFolder("/Конструктор карт Яндекса/Экспорт/")
        print(files)
    except Exception as ex:
        print(ex.args)